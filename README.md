# Compare ini files by their keys

This is a simple script aimed to compare two ini files by their keys. 

Meaning, despite the actual values, have both files the same keys?

## What it does?

Let's compare these two ini files:

```
[section1]
hostname = martinkaradagian.com
rootpass = dontsayit
```

```
[section1]
hostname = lamomiablanca.com
rootpass = dontsayitplease
poweruser = elhombredelabarradehielo
```

If you need to debug an app, that in different servers will get different values... but that needs the same keys in both of them... compare just the keys!

Here the only difference is the `poweruser` key...

Run the script:

`compare-ini-keys.sh file1.ini file2.ini`

Well, this is it... it ain't much, but it's honest work.

![honest-work](./images/work.jpg)

# Argbash

This script was made using [Argbash](https://argbash.io/)... you have a base (`m4` file) and the actual `sh`.

To modify it:

  - modify the `m4` file
  - run this command: `argbash compare-ini-keys.m4 -o compare-ini-keys.sh`
  - *voilà*, you have your script done
 
